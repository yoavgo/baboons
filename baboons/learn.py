def score(fv,weights):
  return sum((weights[f] for f in fv))

def predict(fv,weights):
  return 1 if (score(fv,weights) > 0) else 0

def update(fv,weights,amnt):
  for f in fv:
    weights[f] = weights[f]+amnt

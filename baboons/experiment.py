from collections import defaultdict
import random

import data
from learn import *

## First parameter is the random seed.  This controls the shufling order.
## Second parameter is monkey name. If a name is given, we use only words
## presented to this monkey. Otherwise, we use words presented to all monkeys.
sample = data.rand_sample(10, 'VIOLETTE')

def unigrams(w):
  return [c for c in w]

def loc_unigrams(w):
  return ["%s:%s" % (p,c) for p,c in enumerate(w)]

def first_bigram(w):
  return [(w[0],w[1])]

def second_bigram(w):
  return [(w[1],w[2])]

def first_unigram(w):
  return [w[0]]

def all_bigrams(w):
  return [(w[0],w[1]), (w[1],w[2]), (w[2],w[3])]

## change the following line to try out different representations.
#features = loc_unigrams
#features = second_bigram
#features = first_and_last_unigrams
#features = unigrams
features = all_bigrams

weights = defaultdict(int)
good = bad = 0.0
wgood = wbad = nwgood = nwbad = 0.0000001
seen = set()
print "number of predictions, overall accuracy, non-word accuracy, real-word accuracy"
for item in sample:
  w = item['w']
  fv = features(w)
  pred = predict(fv,weights)
  if pred == item['class']:
    good += 1
    if item['class'] == 1: 
      wgood += 1

    else: nwgood += 1
  else:
    bad += 1
    if item['class'] == 1: wbad += 1
    else: nwbad += 1
    if pred == 0: update(fv,weights,1)
    else: update(fv,weights,-1)
  print good+bad, good/(good+bad), nwgood/(nwgood+nwbad), wgood/(wgood+wbad)
